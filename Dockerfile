FROM ubuntu:18.04

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev

COPY app/ /opt/app

WORKDIR /opt/app

RUN pip install -r requirements.txt

EXPOSE 5000

ENTRYPOINT gunicorn --bind 0.0.0.0:5000 api:app

